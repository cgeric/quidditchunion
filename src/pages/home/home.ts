import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TeamsProvider } from '../../providers/teams/teams';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //positions:
  /*
  Seeker -> see
  Beater -> bea
  Chaser -> cha
  Keeper -> kee
  */
  positions = {
    "see": "Seeker",
    "bea": "Beater",
    "cha": "Chaser",
    "kee": "Keeper"
  }
  attackers: any;
  defenders: any;
  match = {
    id: 10110111,
    snitchChance: 0,
    log: "",
    scoreLog:[],
    teams: []
  }
  teams = [];
  constructor(public navCtrl: NavController,private teamsProvider : TeamsProvider) {
    this.teamsProvider.generateTeams().subscribe((res)=>{
      var teams = res;
      this.teams = teams;
    });
    

  }
  ionViewDidEnter() {
    //this.beginMatch();
  }

  generateMatch(){
    this.match.teams= [];
    this.match.snitchChance = 0;
    this.match.scoreLog = [];
    this.match.log = "";
    var team1 = this.teams[Math.floor(Math.random() * this.teams.length) + 0];
    var team2 = this.teams[Math.floor(Math.random() * this.teams.length) + 0];
    while(team1.id == team2.id){
      team2 = this.teams[Math.floor(Math.random() * this.teams.length) + 0];
    }
    team1.score = 0;
    team2.score = 0;
    this.match.teams.push(team1);
    this.match.teams.push(team2);
    this.beginMatch();
  }

  beginMatch() {
    var startAttack = Math.floor(Math.random() * 2) + 1;
    startAttack += -1;
    console.log(startAttack);
    var startDefense = 0;
    if (startAttack == 1) {

    } else {
      startDefense = 1;
    }
    this.attackers = this.match.teams[startAttack];
    this.defenders = this.match.teams[startDefense];
    this.match.log += "The match begins! <br />";
    this.beginAttack();
  }

  beginAttack() {
    this.match.log += "</span><span style='color:"+this.attackers.color+";'>"+ this.attackers.name + " get the ball! <br />";
    var path = Math.floor((Math.random() * 100) + 5);
    var chasers = [];
    this.attackers.players.forEach(player => {
      if (player.position == "cha") {
        chasers.push(player);
      }
    });
    var chaser = Math.floor((Math.random() * chasers.length));
    if (path <= 20) {
      //slow run! ball lost!
      this.match.log += chasers[chaser].name + " tried to run away but lost the ball!<br />";
      let neutral = this.attackers;
      this.attackers = this.defenders;
      this.defenders = neutral;
      this.beginAttack();
    } else if (path <= 40) {
      //quick run! face the beaters
      this.match.log += chasers[chaser].name + " has the quaffle! flies like a thunderbolt!<br />";
      this.attackChasers();
    } else {
      //normal run! face the beaters
      this.attackBeaters();
    }
  }

  attackChasers() {
    var chasers1 = [];
    this.attackers.players.forEach(player => {
      if (player.position == "cha") {
        chasers1.push(player);
      }
    });
    var chasers2 = [];
    this.defenders.players.forEach(player => {
      if (player.position == "cha") {
        chasers2.push(player);
      }
    });
    var chaser1 = chasers1[Math.floor((Math.random() * chasers1.length) + 0)];
    var chaser2 = chasers2[Math.floor((Math.random() * chasers2.length) + 0)];
    var chaser1Score = Math.floor((Math.random() * 100) + 1) + chaser1.spd;
    var chaser2Score = Math.floor((Math.random() * 100) + 1) + chaser2.spd;
    if(chaser1Score > chaser2Score){
      //evaded chasers!
      this.match.log += chaser1.name + " got trough the last line of defense!<br />";
      this.attackKeeper();
    }else {
      //lost ball 
      this.match.log += chaser2.name + " took the quaffle from "+chaser1.name+" in a tackle!<br />";
      let neutral = this.attackers;
      this.attackers = this.defenders;
      this.defenders = neutral;
      this.beginAttack();
    }
  }

  attackBeaters() {
    var chasers = [];
    this.attackers.players.forEach(player => {
      if (player.position == "cha") {
        chasers.push(player);
      }
    });
    var chaser = chasers[Math.floor((Math.random() * chasers.length) + 0)];
    this.match.log += chaser.name + " has the quaffle, but the beaters are on their way!<br />";
    var beaters = [];
    this.defenders.players.forEach(player => {
      if (player.position == "bea") {
        beaters.push(player);
      }
    });
    var beater = beaters[Math.floor((Math.random() * beaters.length) + 0)];
    //this.match.log += beater.name + " beats the bludger!<br />";

    var chaserScore = Math.floor((Math.random() * 100) + 1) + chaser.spd + chaser.rfx;
    var beaterScore = Math.floor((Math.random() * 100) + 1) + beater.str;
    if(chaserScore > beaterScore){
      //successful evade
      this.match.log += chaser.name + " runs away from the beaters!<br />";
      var path = Math.floor((Math.random() * 100) + 1);
      if(path <= 40){
        this.attackChasers();
      }else{
        this.attackKeeper();
      }
    }else{
      //lost ball
      this.match.log += beater.name + "'s bludger hits "+chaser.name+"!<br />";
      let neutral = this.attackers;
      this.attackers = this.defenders;
      this.defenders = neutral;
      this.beginAttack();
    }
  }

  attackKeeper() {
    var chasers = [];
    this.attackers.players.forEach(player => {
      if (player.position == "cha") {
        chasers.push(player);
      }
    });
    var chaser = chasers[Math.floor((Math.random() * chasers.length) + 0)];
    
    var keeper = null;
    this.defenders.players.forEach(player => {
      if (player.position == "kee") {
        keeper = player;
      }
    });
    this.match.log += chaser.name + " faces "+keeper.name+"!<br />";

    var chaserScore = Math.floor((Math.random() * 100) + 1) + chaser.spd + chaser.str;
    var keeperScore = Math.floor((Math.random() * 100) + 1) + keeper.str + keeper.rfx;

    if(chaserScore > keeperScore){
      this.match.log += chaser.name+" scores! 10 points for "+this.attackers.name+" <br />";
      this.attackers.score += 10;
      this.match.scoreLog.push({
        player: chaser,
        team: this.attackers.id,
        teamName: this.attackers.name,
        teamColor: this.attackers.color,
        teamSecondary : this.attackers.secondary,
        oppoColor: this.defenders.color,
        scored: 10,
        team0Score: this.match.teams[0].score,
        team1Score: this.match.teams[1].score
      });
      this.match.log += "The counter is "+this.match.teams[0].name+" " + this.match.teams[0].score + " - "+this.match.teams[1].name+" " + this.match.teams[1].score +" <br />";
      let neutral = this.attackers;
      this.attackers = this.defenders;
      this.defenders = neutral;
      this.match.snitchChance += 5;
      this.checkSnitch();
    }else{
      this.match.log += keeper.name+" stops the quaffle with one hand! what an amazing keeper! <br />";
      let neutral = this.attackers;
      this.attackers = this.defenders;
      this.defenders = neutral;
      this.beginAttack();
    }

  }

  checkSnitch() {
    var snitchChance = Math.floor((Math.random() * (this.attackers.score+this.defenders.score)) + this.match.snitchChance);
    console.log(snitchChance+"%");
    if(snitchChance > 150){
      this.match.log += " Wait, is that the Snitch? <br />";
      this.attackSeeker();
    }else{
      this.beginAttack();
    }
  }

  attackSeeker(){
    var seeker1 = null;
    this.attackers.players.forEach(player => {
      if (player.position == "see") {
        seeker1 = player;
      }
    });
    var seeker2 = null;
    this.defenders.players.forEach(player => {
      if (player.position == "see") {
        seeker2 = player;
      }
    });
    if(seeker1.spd>seeker2.spd){
      this.match.log += seeker1.name + " is following the snitch, with "+seeker2.name+" right on it's tail! <br />";
    }else{
      this.match.log += seeker2.name + " is trailing the snitch, with "+seeker1.name+" on it's feet! <br />";
    }
    var seeker1Score = Math.floor((Math.random() * 100) + 1) + seeker1.spd + seeker1.rfx;
    var seeker2Score = Math.floor((Math.random() * 100) + 1) + seeker2.spd + seeker2.rfx;
    
    if(seeker1Score > seeker2Score){
      this.match.log += seeker1.name + " closes the hand, that's it! 'has the snitch! <br />";
      this.attackers.score += 150;
      this.attackers.snitchCatched = true;
      this.match.scoreLog.push({
        player: seeker1,
        team: this.attackers.id,
        teamName: this.attackers.name,
        teamColor: this.attackers.color,
        teamSecondary : this.attackers.secondary,
        oppoColor: this.defenders.color,
        scored: 150,
        team0Score: this.match.teams[0].score,
        team1Score: this.match.teams[1].score
      });
    }else{  
      this.match.log += seeker1.name + " makes a swift movement, and.... amazing! got it! <br />";
      this.defenders.score += 150;
      this.defenders.snitchCatched = true;
      this.match.scoreLog.push({
        player: seeker2,
        team: this.defenders.id,
        teamName: this.defenders.name,
        teamColor: this.defenders.color,
        teamSecondary : this.defenders.secondary,
        oppoColor: this.attackers.color,
        scored: 150,
        team0Score: this.match.teams[0].score,
        team1Score: this.match.teams[1].score
      });
    }
    this.match.log += "</span>The match is over! <br />";
    this.match.log += "The final counter is "+this.match.teams[0].name+" " + this.match.teams[0].score + " - "+ this.match.teams[1].name+" " + this.match.teams[1].score +" <br />";
    if(this.attackers.score > this.defenders.score){
      if(this.attackers.snitchCatched == true){
        this.match.log += this.attackers.name + " won thanks to their hardwork and their seeker! <br />";
      }else{
        this.match.log += this.attackers.name + " won even when the other team got the snitch! <br />";
      }
    }else{
      if(this.defenders.snitchCatched == true){
        this.match.log += this.defenders.name + " won thanks to their hardwork and their seeker! <br />";
      }else{
        this.match.log += this.defenders.name + " won even when the other team got the snitch! <br />";
      } 
    }
    
  }
  getColor(team){
    //console.log(team);
    return team.color;
  }
  getSecondary(team){
    //console.log(team);
    return team.secondary;
  }
}
