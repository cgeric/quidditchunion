import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the TeamsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TeamsProvider {
  teams = [];
  names = [
    "Andrew",
    "Allan",
    "Angus",
    "Anna",
    "Andrea",
    "Albit",
    "Arnold",
    "Asmodia",
    "Arendelle",
    "Anabelle",
    "Bob",
    "Bill",
    "Bernard",
    "Beatrix",
    "Bitha",
    "Bronn",
    "Bartimus",
    "Bartholomew",
    "Barty",
    "Charles",
    "Charlie",
    "Coral",
    "Cedric",
    "Cyntiah",
    "Caroline",
    "Celine",
    "Dimitry",
    "Dean",
    "Djovan",
    "Dylan",
    "Diego",
    "Danessa",
    "Eric",
    "Eldric",
    "Eren",
    "Fred",
    "Frederich",
    "Francis",
    "Foul",
    "René",
    "Randy",
    "Rudolph",
    "Thomas",
    "Tom",
    "tod",
    "Terry",
    "Tim",
    "Timothy",
    "Cassandra",
    "candace",
    "William",
    "George",
    "Georgina",
    "Jessie",
    "Jessica",
    "Jean",
    "Wanda",
    "Zachary",
    "Ginny",
    "Lilly",
    "Petunia",
    "Blossom",
    "Luna"
]
  surnames = [
    "Abbey",
    "Abbot",
    "Adams",
    "Andrews",
    "Arnold",
    "Barch",
    "Bleak",
    "Barnaby",
    "Bertrand",
    "Boggs",
    "Blyme",
    "Calvin",
    "Cairn",
    "Calton",
    "Cassius",
    "Cynth",
    "Devon",
    "Dustin",
    "Edwards",
    "Edwin",
    "Eldritch",
    "Eldon",
    "Engel",
    "Francis",
    "Frederickson",
    "Fredson",
    "Filing",
    "O'Ryan",
    "O'Connor",
    "O'Hara",
    "O'Ryley",
    "Percival",
    "Proudhouse",
    "Prentish",
    "Prinston",
    "Pearson",
    "Potting",
    "Perch",
    "Porch",
    "Williams",
    "Winston",
    "Warren",
    "Zachary"
]  
constructor(public http: HttpClient) {
    console.log('Hello TeamsProvider Provider');
  }

  generateTeams(){
    this.teams = [];
    return this.http.get("../../assets/json/teams.json").map((res : any)=>{
      res.forEach(team => {
        team.score = 0;
        team.snitchCatched = false;
        team.players = this.createPlayers();
      });
      return res;
    });
  }

  createPlayers(){
    var players = [];
    players.push({ 
      id:  Math.floor(Math.random() * 4022092) + 1022092,
      name: this.names[(Math.floor(Math.random() * this.names.length) + 1 )] + " " + this.surnames[(Math.floor(Math.random() * (this.surnames.length-1)) +1 )],
      position: "kee",
      number: 1,
      spd: 5,
      str: 5,
    rfx: 5});
    players.push({ 
      id:  Math.floor(Math.random() * 4022092) + 1022092,
      name: this.names[(Math.floor(Math.random() * this.names.length) + 1 )] + " " + this.surnames[(Math.floor(Math.random() * (this.surnames.length-1)) +1 )],
      position: "bea",
      number: 2,
      spd: 5,
      str: 5,
    rfx: 5});
    players.push({ 
      id:  Math.floor(Math.random() * 4022092) + 1022092,
      name: this.names[(Math.floor(Math.random() * this.names.length) + 1 )] + " " + this.surnames[(Math.floor(Math.random() * (this.surnames.length-1)) +1 )],
      position: "bea",
      number: 3,
      spd: 5,
      str: 5,
    rfx: 5});
    players.push({ 
      id:  Math.floor(Math.random() * 4022092) + 1022092,
      name: this.names[(Math.floor(Math.random() * (this.names.length-1)) + 1 )] + " " + this.surnames[(Math.floor(Math.random() * (this.surnames.length-1)) +1 )],
      position: "cha",
      number: 4,
      spd: 5,
      str: 5,
    rfx: 5});
    players.push({ 
      id:  Math.floor(Math.random() * 4022092) + 1022092,
      name: this.names[(Math.floor(Math.random() * (this.names.length-1)) + 1 )] + " " + this.surnames[(Math.floor(Math.random() * (this.surnames.length-1)) +1 )],
      position: "cha",
      number: 5,
      spd: 5,
      str: 5,
    rfx: 5});
    players.push({ 
      id:  Math.floor(Math.random() * 4022092) + 1022092,
      name: this.names[(Math.floor(Math.random() * (this.names.length-1)) + 1 )] + " " + this.surnames[(Math.floor(Math.random() * (this.surnames.length-1)) +1 )],
      position: "cha",
      number: 6,
      spd: 5,
      str: 5,
    rfx: 5});
    players.push({ 
      id:  Math.floor(Math.random() * 4022092) + 1022092,
      name: this.names[(Math.floor(Math.random() * this.names.length) + 1 )] + " " + this.surnames[(Math.floor(Math.random() * (this.surnames.length-1)) +1 )],
      position: "see",
      number: 7,
      spd: 5,
      str: 5,
    rfx: 5});
    return players;
  }
}
